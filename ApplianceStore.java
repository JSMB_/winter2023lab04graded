import java.util.Scanner;

public class ApplianceStore {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("How many ovens will you purchase?:");
		int ovenAmount = sc.nextInt();

		//Unecessary for the newer implementations
		//Oven specs = new Oven();

		Oven[] ovens = new Oven[ovenAmount];

		int lastArrPosition = 0;
		//enter the oven(s) specs - also part of the previous lab sutff
		for(int i = 0; i < ovens.length; i++) {
			ovens[i] = new Oven("ARFaSyS", "Black", 235);
			//print all fields(newest)
			/*System.out.println(ovens[i].getModel());
			System.out.println(ovens[i].getColour());
			System.out.println(ovens[i].getMaxTemperature());*/

			lastArrPosition++;
			if(i < ovens.length) {
				lastArrPosition -= 1;
			}

			//deleted setter method calls
			/*System.out.println("Please enter the oven's model: ");
			ovens[i].setModel(sc.next());
			System.out.println("Please enter the oven's colour: ");
			ovens[i].setColour(sc.next());
			System.out.println("Please enter the oven's maximum temperature: ");
			ovens[i].setMaxTemperature(sc.nextInt());*/
		}

		//last field print pre-modification
		System.out.println(ovens[lastArrPosition].getModel());
		System.out.println(ovens[lastArrPosition].getColour());
		System.out.println(ovens[lastArrPosition].getMaxTemperature());
		//modified last field
		ovens[lastArrPosition].setColour("Blue");
		ovens[lastArrPosition].setMaxTemperature(500);
		//print modified last field
		System.out.println(ovens[lastArrPosition].getModel());
		System.out.println(ovens[lastArrPosition].getColour());
		System.out.println(ovens[lastArrPosition].getMaxTemperature());

		//Old print
		/*System.out.println(ovens[0].getModel());
		System.out.println(ovens[0].getColour());
		System.out.println(ovens[0].getMaxTemperature());*/

		//previous lab stuff
		/*System.out.println(ovens[3].getModel());
		System.out.println(ovens[3].getColour());
		System.out.println(ovens[3].getMaxTemperature());

		ovens[0].printModel();
		specs.isMaxTempHigh(ovens[0].maxTemperature);*/
	}
}