public class Oven {
	private String model;
	private String colour;
	private int maxTemperature;
	
	
	//CONSTRUCTORS
	public Oven(String model, String colour, int maxTemperature) {
		this.model = model;
		this.colour = colour;
		this.maxTemperature = maxTemperature;
	}
	
	
	//METHODS
	public void printModel() {
		System.out.println("This oven's model is: " + model);
	}
	
	public void isMaxTempHigh(int temp) {
		if(temp < 235) {
			System.out.println("This oven's maximum temperature is normal.");
		} else {
			System.out.println("This oven's maximum temperature is above average!");
		}
	}
	
	//deleted setter method
	/*public void setModel(String model) {
		this.model = model;
	}*/
	
	public void setMaxTemperature(int maxTemperature) {
		this.maxTemperature = maxTemperature;
	}
	
	public void setColour(String colour) {
		colour.toLowerCase();
		
		if(maxTemperature >= 235 && colour == "white") {
			setColourDenyMessage(this.maxTemperature, colour);
		} else if (maxTemperature >= 50 && colour == "black") {
			setColourDenyMessage(this.maxTemperature, colour);
		} else {
			this.colour = colour;
		}
	}
	
	public String getModel() {
		return this.model;
	}
	
	public String getColour() {
		return this.colour;
	}
	
	public int getMaxTemperature() {
		return this.maxTemperature;
	}
	
	//helper methods
	private static void setColourDenyMessage(int maxTemperature, String colour) {
		System.out.println("Ovens with a heat capacity of " + maxTemperature + " Celsius may not be coloured " + colour + "!");
	}
}